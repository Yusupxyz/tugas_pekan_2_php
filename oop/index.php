
<?php
include ('animal.php');
include ('frog.php');
include ('ape.php');

$sheep = new Animal("shaun");

echo "Name : $sheep->name <br/>"; // "shaun"
echo "legs : $sheep->legs <br/>"; // 4
echo "cold blooded : $sheep->cold_blooded <br/><br/>"; // "no"

$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"

$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"

?>